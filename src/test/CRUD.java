package test;


import Entity.Employee;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class CRUD {

    public  void createData(String first, String last, String company) {
        //create session factory
        SessionFactory factory = new Configuration().configure("hibernate.cfg.xml")
                .addAnnotatedClass(Employee.class)
                .buildSessionFactory();

        //create a session
        Session session = factory.getCurrentSession();

        try{
            //create 3 student objects
            System.out.println("Creating 1 employee objects...");
            Employee tempEmployee = new Employee(first, last, company);

            //start a transaction
            session.beginTransaction();
            System.out.println("Beginning transaction...");

            //save the student object
            session.save(tempEmployee);
            System.out.println("Saving the new employee...");

            //commit the transaction
            session.getTransaction().commit();
            System.out.println("Done!");
        } finally {
            factory.close();
        }
    }
    public void read(String first, String last, String company){
        //create session factory this is for hibernate
        SessionFactory factory = new Configuration().configure("hibernate.cfg.xml")
                .addAnnotatedClass(Employee.class)
                .buildSessionFactory();

        //create a session this is for hibernate
        Session session = factory.getCurrentSession();

        try{
            //create a new student object
            System.out.println("Creating new employee object...");
            Employee tempEmployee = new Employee(first, last, company);

            //start a transaction
            session.beginTransaction();
            System.out.println("Beginning transaction...");

            //save the student object
            System.out.println("Saving the new employee...");
            System.out.println(tempEmployee);
            session.save(tempEmployee);

            //commit the transaction
            session.getTransaction().commit();

            //***********************************************************************************
            //***Code for Reading the object***
            //find out the student's id: primary key
            System.out.println("Saved employee. Generated id: " + tempEmployee.getId());

            //Get a new session and start a transaction
            session = factory.getCurrentSession();
            session.beginTransaction();

            //Retrieve student based on the id: primary key
            System.out.println("\nGetting student with id: " + tempEmployee.getId());
            Employee myEmployee = session.get(Employee.class, tempEmployee.getId());
            System.out.println("Get Completed: " + myEmployee);

            //commit the transaction
            session.getTransaction().commit();
            System.out.println("Done!");
        } finally {
            factory.close();
        }
    }

    public void update(){

        //create session factory this is for hibernate
        SessionFactory factory = new Configuration().configure("hibernate.cfg.xml")
                .addAnnotatedClass(Employee.class)
                .buildSessionFactory();

        //create a session this is for hibernate
        Session session = factory.getCurrentSession();

        try{
            int employeeId = 12;
            //Get a new session and start a transaction
            session = factory.getCurrentSession();
            session.beginTransaction();

            //Retrieve student based on the id: primary key
            System.out.println("\nGetting student with id: " + employeeId);
            Employee myEmployee = session.get(Employee.class, employeeId);

            //Updating the student first Name at primary key 1
            System.out.println("Updating Employee...");
            myEmployee.setFirstName("Otto");

            //commit the transaction
            session.getTransaction().commit();

            //*********************************************************//
            //Create new factory session to do a bulk update
            session = factory.getCurrentSession();
            session.beginTransaction();

            //Update email for all students
            System.out.println("Updating email for all the students...");
            session.createQuery("update Employee set company='revature'").executeUpdate();
            //or you can use a where clause to be more precise
            session.createQuery("update Employee set company='pyramid' where company='revature'").executeUpdate();

            //commit the transaction
            session.getTransaction().commit();
            System.out.println("Done!");
        } finally {
            factory.close();

        }
        }


    public void delete(int employeeID){
        //create session factory this is for hibernate
        SessionFactory factory = new Configuration().configure("hibernate.cfg.xml")
                .addAnnotatedClass(Employee.class)
                .buildSessionFactory();

        //create a session this is for hibernate
        Session session = factory.getCurrentSession();

        try{

            //Get a new session and start a transaction
            session = factory.getCurrentSession();
            session.beginTransaction();

            //Retrieve student based on the id: primary key
            System.out.println("\nGetting student with id: " + employeeID);
            Employee myEmployee = session.get(Employee.class, employeeID);

            //Delete the student
            //System.out.println("Deleting student: " + myStudent);
            //session.delete(myStudent);

            //Delete student where id=15 this allows you to delete on the fly instead of having to retrieve the object.
            System.out.println("Deleting Employee where id= " + employeeID);
            session.createQuery("delete from Employee where id= " + employeeID).executeUpdate();

            //commit the transaction
            session.getTransaction().commit();
            System.out.println("Done!");
        } finally {
            factory.close();
        }
    }

    public void query(){
        //create session factory
        SessionFactory factory = new Configuration().configure("hibernate.cfg.xml")
                .addAnnotatedClass(Employee.class)
                .buildSessionFactory();

        //create a session
        Session session = factory.getCurrentSession();

        try{
            //start a transaction
            session.beginTransaction();

            //Query Student: All Students
            List<Employee> theEmployee = session.createQuery("from Employee").getResultList();
            //Display Students
            System.out.println("Displaying all the Students...");
            displayEmployee(theEmployee);

            //Query Student: lastName='Skywalker'
            theEmployee = session.createQuery("from Employee s where s.lastName='Gates'").getResultList();
            //Display results
            System.out.println("\nDisplaying all the Students with last name of Skywalker.");
            displayEmployee(theEmployee);

            //Query Student: lastName='Skywalker' OR firstName='Ellie'
            theEmployee = session.createQuery("from Employee s where lastName='Trump' OR firstName='Michael'").getResultList();
            //Display results
            System.out.println("\nDisplaying all the students with last name Trump or first name Michael...");
            displayEmployee(theEmployee);

            //Query Student: where email LIKE '%too@gmail.com'
            theEmployee = session.createQuery("from Employee s where s.company LIKE '%pyramid'").getResultList();
            //Display results
            System.out.println("\nDisplaying all the employees with the pyramid company address...");
            displayEmployee(theEmployee);

            //commit the transaction
            session.getTransaction().commit();
            System.out.println("Done!");
        } finally {
            factory.close();
        }
    }

    private static void displayEmployee(List<Employee> theEmployee) {
        for(Employee tempEmployee : theEmployee) {
            System.out.println(tempEmployee);
        }
    }

}
