package test;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        CRUD c = new CRUD();
        String answer;
        Scanner in = new Scanner(System.in);

        System.out.println("Which crud feature would you like to use:");
        answer = in.nextLine();

        if (answer.equals("create")){
            c.createData("Michael","Jordan", "Wakanda");
        }
        if (answer.equals("read")){
            c.read("Bill", "gates","Microsoft");
        }
        if (answer.equals("update")){
            c.update();
        }
        if (answer.equals("delete")){
            c.delete(10);
        }
        if (answer.equals("query")){
            c.query();
        }


    }
}
